/**
 * View Models used by Spring MVC REST controllers.
 */
package org.uom.fit.airlinedatamanager.web.rest.vm;
