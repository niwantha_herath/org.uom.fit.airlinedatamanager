/**
 * Spring Data ElasticSearch repositories.
 */
package org.uom.fit.airlinedatamanager.repository.search;
