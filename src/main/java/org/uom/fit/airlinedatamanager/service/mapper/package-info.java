/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package org.uom.fit.airlinedatamanager.service.mapper;
