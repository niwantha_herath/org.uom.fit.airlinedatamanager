(function() {
    'use strict';

    angular
        .module('airlinedatamanagerApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
